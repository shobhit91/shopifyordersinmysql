<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class orders extends Model
{
	public static function insertOrders($orderData){
	  $order_id = $orderData['order_number'];
	  $email_id = $orderData['email'];
	  $total = $orderData['total_price'];
	  $currency = $orderData['currency'];
	  $financial_status = $orderData['financial_status'];
	  $confirmed = $orderData['confirmed'];
	  $created_at = date('Y-m-d', strtotime($orderData['created_at']));
	  
	
		$order = new Orders;
		$order->order_id = $order_id;
		$order->email_id = $email_id;
		$order->total = $total;
		$order->currency = $currency;
		$order->financial_status = $financial_status;
		$order->created_at = $created_at;
		$order->save();
	}
	
	public static function getCustAvgMean($email_id){
		$order = new Orders;
		$getOrdersByEmail = $order->where('email_id',$email_id)
		->selectRaw('GROUP_CONCAT(total) as total')
		->get();
		$avgMean = '';
		if($getOrdersByEmail){
			foreach($getOrdersByEmail as $price){
				$prices = $price['total'];
				$exp = explode(',', $prices);
				$count = count($exp);
				if($count){
					for($i=0; $i<$count; $i++){
						$avgMean += $exp[$i]/$count;
					}
				}
			}
		}
		return number_format($avgMean,2);
	}
}
