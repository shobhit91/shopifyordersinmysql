<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;

class OrderController extends Controller
{
	public function getOrders()
    {
		/*$url=base_path()."/orders.json";
		$result = file_get_contents($url);
		$data = json_decode($result, true);
		foreach($data as $orders){		  
		  //Orders::insertOrders($orders);
		}*/
		$orders = Orders::all();
		$price = '0';
		foreach($orders as $order){
			$price +=$order['total'];
			$order['avgMean'] = Orders::getCustAvgMean($order['email_id']);
		}
		$data = array();
		$count = count($orders);
		$data['count'] = $count;
		$data['data'] = $orders;
		$data['price'] = $price;
		$data['allOrdersMean'] = $price/$count;
		return view('orders')->with('orders', $data);
    }
}
