-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2019 at 06:23 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopify`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` int(25) NOT NULL,
  `email_id` varchar(256) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `currency` varchar(256) NOT NULL,
  `financial_status` varchar(256) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `email_id`, `total`, `currency`, `financial_status`, `created_at`, `updated_at`) VALUES
(1, 1149, 'Larue.Graham@developer-tools.shopifyapps.com', '68.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:06'),
(2, 1148, 'Alvena.Bayer@developer-tools.shopifyapps.com', '77.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:06'),
(3, 1147, 'Aliya.Doyle@developer-tools.shopifyapps.com', '344.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:06'),
(4, 1146, 'Naomie.Kirlin@developer-tools.shopifyapps.com', '132.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:06'),
(5, 1145, 'Christopher.Kohler@developer-tools.shopifyapps.com', '256.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(6, 1144, 'Rory.Murray@developer-tools.shopifyapps.com', '31.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(7, 1143, 'Aditya.Bruen@developer-tools.shopifyapps.com', '192.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(8, 1142, 'Oma.Glover@developer-tools.shopifyapps.com', '292.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(9, 1141, 'Fermin.Barrows@developer-tools.shopifyapps.com', '38.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(10, 1140, 'Aliya.Doyle@developer-tools.shopifyapps.com', '68.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(11, 1139, 'Esther.Greenholt@developer-tools.shopifyapps.com', '74.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(12, 1138, 'Esther.Greenholt@developer-tools.shopifyapps.com', '75.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(13, 1137, 'Danika.Raynor@developer-tools.shopifyapps.com', '72.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(14, 1136, 'Alan.Schultz@developer-tools.shopifyapps.com', '240.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(15, 1135, 'Autumn.Champlin@developer-tools.shopifyapps.com', '56.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(16, 1134, 'Brennon.Pollich@developer-tools.shopifyapps.com', '176.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(17, 1133, 'Clemmie.Fisher@developer-tools.shopifyapps.com', '216.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(18, 1132, 'Erin.Kihn@developer-tools.shopifyapps.com', '42.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(19, 1131, 'Marielle.Walker@developer-tools.shopifyapps.com', '34.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(20, 1130, 'Alvena.Bayer@developer-tools.shopifyapps.com', '316.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(21, 1129, 'Keira.Adams@developer-tools.shopifyapps.com', '68.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(22, 1128, 'Giuseppe.Harris@developer-tools.shopifyapps.com', '297.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(23, 1127, 'Reuben.Kreiger@developer-tools.shopifyapps.com', '107.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(24, 1126, 'Lenora.McLaughlin@developer-tools.shopifyapps.com', '38.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(25, 1125, 'Fermin.Barrows@developer-tools.shopifyapps.com', '36.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(26, 1124, 'Ericka.Dickens@developer-tools.shopifyapps.com', '115.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(27, 1123, 'Elva.King@developer-tools.shopifyapps.com', '193.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(28, 1122, 'Dianna.McCullough@developer-tools.shopifyapps.com', '172.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(29, 1121, 'Lacy.Cummings@developer-tools.shopifyapps.com', '147.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:07'),
(30, 1120, 'Eusebio.Jerde@developer-tools.shopifyapps.com', '51.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(31, 1119, 'Stone.Cassin@developer-tools.shopifyapps.com', '34.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(32, 1118, 'Lacy.Cummings@developer-tools.shopifyapps.com', '62.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(33, 1117, 'Stephon.Stamm@developer-tools.shopifyapps.com', '66.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(34, 1116, 'Reinhold.Ward@developer-tools.shopifyapps.com', '73.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(35, 1115, 'Winona.Lakin@developer-tools.shopifyapps.com', '228.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(36, 1114, 'Herta.Mraz@developer-tools.shopifyapps.com', '75.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(37, 1113, 'Dedric.Fritsch@developer-tools.shopifyapps.com', '162.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(38, 1112, 'Danika.Raynor@developer-tools.shopifyapps.com', '225.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(39, 1111, 'Julie.Pagac@developer-tools.shopifyapps.com', '204.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(40, 1110, 'Dedric.Fritsch@developer-tools.shopifyapps.com', '72.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(41, 1109, 'Oren.Lowe@developer-tools.shopifyapps.com', '205.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(42, 1108, 'Stephon.Stamm@developer-tools.shopifyapps.com', '71.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(43, 1107, 'Lenora.McLaughlin@developer-tools.shopifyapps.com', '121.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(44, 1106, 'Reinhold.Ward@developer-tools.shopifyapps.com', '180.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(45, 1105, 'Eriberto.Cummerata@developer-tools.shopifyapps.com', '68.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(46, 1104, 'Oma.Glover@developer-tools.shopifyapps.com', '191.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(47, 1103, 'Julie.Pagac@developer-tools.shopifyapps.com', '41.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(48, 1102, 'Lorna.Crist@developer-tools.shopifyapps.com', '474.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(49, 1101, 'Esther.Greenholt@developer-tools.shopifyapps.com', '229.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08'),
(50, 1100, 'Emerson.Yundt@developer-tools.shopifyapps.com', '78.00', 'GBP', 'paid', '2019-01-21 00:00:00', '2019-01-27 14:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shobhit Kumar', 'shobhit18@ymail.com', '$2y$10$AjO5QvB0MkrBkBgVVBxTi.psMTcscU3CWWELdda9WRWi2O8Bcjziu', 'CDSd2p4nbcniou1ojBmhMv7m1JhAVt04HpWbSHFW9ftc5FXwoOqw26wYPJxG', '2019-01-26 05:31:15', '2019-01-26 05:31:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
