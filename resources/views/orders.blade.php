@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Total Orders {{$orders['count']}}</div>
                <div class="panel-heading">All Orders Mean GBP {{$orders['allOrdersMean']}}</div>
				<table class="orders">
						<tr>
						<th>Sr. No.</th>
						<th>Order ID</th>
						<th>Email</th>
						<th>Avg Mean</th>
						<th>Total</th>
						<th>Status</th>
						<th>Created Date</th>
						</tr>
					<tbody>
						@foreach ($orders['data'] as $order)
						<tr>
							<td>{{ $loop->iteration }}.</td>
							<td>{{ $order['order_id']}}</td>
							<td>{{ strtolower($order['email_id'])}}</td>
							<td>{{ $order['currency'].' '. $order['avgMean']}}</td>
							<td>{{ $order['currency'].' '. $order['total']}}</td>
							<td>{{ ucfirst($order['financial_status'])}}</td>
							<td>{{ date('d-M-y', strtotime($order['created_at']))}}</td>	
						</tr>							
						@endforeach
					</tbody>
				</table>
				
				
            </div>
        </div>
    </div>
</div>
@endsection
